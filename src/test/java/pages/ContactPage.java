package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 20.09.2017.
 */
public class ContactPage  {
    WebDriver driver;

    public ContactPage(WebDriver driver){
        this.driver = driver;
    }

//    Locators

    By userName = By.cssSelector("#name");
    By userEmail = By.cssSelector("#email");
    By userPhone = By.cssSelector("#tel");
    By userTheme = By.xpath("html/body/div[1]/main/section/div/div/div[2]/div/div/div[4]/div/select");
    By themeOption = By.xpath("html/body/div/main/section/div/div/div[2]/div/div/div[4]/div/select/option[4]");
    By userMessage = By.xpath("html/body/div/main/section/div/div/div[2]/div/div/div[5]/div/textarea");
    By submitBtn = By.cssSelector(".button.button--bg.button--sizeLg.js-form-submit");

    public void navigateToContactPage(){
        driver.navigate().to("http://1:1@rau24.wezom.net/contact");
    }

    public void setUserName(String strUserName){
        driver.findElement(userName).sendKeys(strUserName);
    }

    public void setUserEmail(String strUserEmail){
        driver.findElement(userEmail).sendKeys(strUserEmail);
    }

    public void setUserPhone(String strUserPhone){
        driver.findElement(userPhone).sendKeys(strUserPhone);
    }

    public void setUserTheme(){
        driver.findElement(userTheme).click();
        driver.findElement(themeOption).click();
    }

    public void setUserMessage(String strUserMessage){
        driver.findElement(userMessage).sendKeys(strUserMessage);
    }

    public void setSubmitBtn(){
        driver.findElement(submitBtn).click();
    }

}
