package tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPopUp;


public class UserLoginTest extends BaseConfig {

    public LoginPopUp userLogining;

    @BeforeMethod
    public void beforeTest(){
        userLogining = new LoginPopUp(driver);
    }

    @Test
    public void authorization() {

        userLogining.setPopUpLogin();
        userLogining.setUserLogin("ivanov.v.wezom@gmail.com");
        userLogining.setUserPassword("123456");
        userLogining.setEnterButton();

        Assert.assertTrue(driver.findElement(By.cssSelector(".bell__head.js-downListLink")).isDisplayed());
    }

}
