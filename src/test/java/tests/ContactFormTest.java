package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ContactPage;

/**
 * Created by ivanov.v on 20.09.2017.
 */
public class ContactFormTest extends BaseConfig {

    public ContactPage userSendsForm;

    @BeforeMethod
    public void beforeTest(){
        userSendsForm = new ContactPage(driver);
    }

    @Test
    public void userFillsOutFields(){
        userSendsForm.navigateToContactPage();
        userSendsForm.setUserName("Testovich");
        userSendsForm.setUserEmail("test@test.test");
        userSendsForm.setUserPhone("999999999");
        userSendsForm.setUserTheme();
        userSendsForm.setUserMessage("Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                "sed do eiusmod tempor incididunt ut labore et dolore magna ");
        userSendsForm.setSubmitBtn();
    }

}
